import React, { PureComponent } from 'react';
class Spinner extends PureComponent{
    render(){
        return(
            <div className="spinner">
                <div>...</div>
            </div>
            
        )
    }
    
}
export default Spinner;